const totalBalance = document.querySelector(".total-balance");
const totalIncome = document.querySelector(".total-income");
const totalOutcome = document.querySelector(".total-outcome");
const incomeElement = document.querySelector("#income");
const outcomeElement = document.querySelector("#outcome");
const allElement = document.querySelector("#all");
const incomeList = document.querySelector("#income .list");
const outcomeList = document.querySelector("#outcome .list");
const alllist = document.querySelector("#all .list");
const pBalance = document.getElementById("balanceTotal");

const incomeBtn = document.querySelector(".tab1");
const expenseBtn = document.querySelector(".tab2");
const allBtn = document.querySelector(".tab3");

const addExpense = document.querySelector(".add-expense");
const expenseTitle = document.getElementById("outcome-title-input");
const expenseValue = document.getElementById("outcome-value-input");

const addincome = document.querySelector(".add-income");
const incomeTitle = document.getElementById("income-title-input");
const incomeValue = document.getElementById("income-value-input");

let walletList;

let balanceAmount = 0,
  incomeAmount = 0,
  outcomeAmount = 0;

const deleteId = "delete";
const edit = "edit";

walletList = JSON.parse(localStorage.getItem("walletList")) || [];
update();

expenseBtn.addEventListener("click", function () {
  show(outcomeElement);
  hide([incomeElement, allElement]);
  active(expenseBtn);
  inactive([incomeBtn, allBtn]);
});

incomeBtn.addEventListener("click", function () {
  show(incomeElement);
  hide([outcomeElement, allElement]);
  active(incomeBtn);
  inactive([expenseBtn, allBtn]);
});

allBtn.addEventListener("click", function () {
  show(allElement);
  hide([incomeElement, outcomeElement]);
  active(allBtn);
  inactive([incomeBtn, expenseBtn]);
});

incomeList.addEventListener("click", deleteOrEdit);
outcomeList.addEventListener("click", deleteOrEdit);
alllist.addEventListener("click", deleteEntry);

function deleteOrEdit(event) {
  const targetBtn = event.target;

  const entryParent = targetBtn.parentNode;
  const entry = entryParent.parentNode;

  if (targetBtn.id === deleteId) {
    deleteEntry(entry);
  } else if (targetBtn.id === edit) {
    editEntry(entry);
  }
}

function deleteEntry(entry) {
  walletList.splice(entry.id, 1);
  update();
}

function editEntry(entry) {
  let ENTRY = walletList[entry.id];
  if (ENTRY.type === "income") {
    incomeValue.value = ENTRY.amount;
    incomeTitle.value = ENTRY.title;
  } else if (ENTRY.type === "expense") {
    expenseValue.value = ENTRY.amount;
    expenseTitle.value = ENTRY.title;
  }
  deleteEntry(entry);
}

function calculateTotal(type, list) {
  let sum = 0;
  list.forEach((entry) => {
    if (entry.type === type) {
      sum += parseFloat(entry.amount);
    }
  });
  return sum;
}

function calculateBalance(income, outcome) {
  return income - outcome;
}

function clearInput(inputs) {
  inputs.forEach((input) => {
    input.value = "";
  });
}

function show(element) {
  element.classList.remove("hide");
}

function hide(elements) {
  elements.forEach((element) => {
    element.classList.add("hide");
  });
}

function active(element) {
  element.classList.add("active");
}

function inactive(elements) {
  elements.forEach((element) => {
    element.classList.remove("active");
  });
}

addExpense.addEventListener("click", function () {
  if (!expenseTitle.value || !expenseValue.value) return;

  const expense = {
    type: "expense",
    title: expenseTitle.value,
    amount: expenseValue.value,
  };
  walletList.push(expense);
  update();
  clearInput([expenseTitle, expenseValue]);
});

addincome.addEventListener("click", function () {
  if (!incomeTitle.value || !incomeValue.value) return;

  const income = {
    type: "income",
    title: incomeTitle.value,
    amount: incomeValue.value,
  };
  walletList.push(income);
  update();
  clearInput([incomeTitle, incomeValue]);
});

function showEntry(list, type, title, amount, id) {
  const entry = `<li id="${id}" class="${type}">
        <h2 class="entry">${title}: $${parseFloat(amount)}</h2>
        <div class="edit-delete-div">
            <img id="edit" src="CSS/edit.png"/>
            <img id="delete" src="CSS/bin.png"/>
        </div>
    <li>`;
  const position = "afterbegin";

  list.insertAdjacentHTML(position, entry);
}

function showEntryAll(list, type, title, amount, id) {
  const entry = `<li id="${id}" class="${type}">
        <h2 class="entry">${title}: $${parseFloat(amount)}</h2>
        <div class="edit-delete-div">
            <img id="delete" src="CSS/bin.png"/>
        </div>
    <li>`;
  const position = "afterbegin";

  list.insertAdjacentHTML(position, entry);
}

function update() {
  const income = calculateTotal("income", walletList);
  const outcome = calculateTotal("expense", walletList);
  const balance = calculateBalance(income, outcome);

  parseFloat(income);
  parseFloat(outcome);
  parseFloat(balance);

  totalBalance.innerHTML = `${balance.toFixed(2)} PLN`;
  totalIncome.innerHTML = `${income.toFixed(2)} PLN`;
  totalOutcome.innerHTML = `${outcome.toFixed(2)} PLN`;
  updateBalance();

  clearElement([outcomeList, incomeList, alllist]);

  walletList.forEach((entry, index) => {
    if (entry.type === "expense") {
      showEntry(outcomeList, entry.type, entry.title, entry.amount, index);
    } else if (entry.type === "income") {
      showEntry(incomeList, entry.type, entry.title, entry.amount, index);
    }
    showEntryAll(alllist, entry.type, entry.title, entry.amount, index);
  });

  localStorage.setItem("walletList", JSON.stringify(walletList));
}

function updateBalance() {
  const income = calculateTotal("income", walletList);
  const outcome = calculateTotal("expense", walletList);
  if (income === outcome) {
    pBalance.innerHTML = "Your Balance is 0 PLN";
  } else if (income > outcome) {
    pBalance.innerHTML = `You can still spent ${income - outcome} PLN`;
  } else if (income < outcome) {
    pBalance.innerHTML = `You are out of pocket by ${outcome - income} PLN`;
  }
}

function clearElement(elements) {
  elements.forEach((element) => {
    element.innerHTML = "";
  });
}
